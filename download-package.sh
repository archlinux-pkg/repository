#!/bin/sh -e

name="$1"

echo "Downloading ${name}..."
if (curl -s -S -L -f -o artifacts.zip --header "JOB-TOKEN: $CI_JOB_TOKEN" "https://gitlab.com/archlinux-pkg/packages/$name/-/jobs/artifacts/master/download?job=build") ; then
	unzip -o artifacts.zip -d public
	echo ""
else
	(>&2 echo "WARNING: download for $name failed!\n")
	exit 0 # just skip this repo to allow for others to complete successfully
fi
