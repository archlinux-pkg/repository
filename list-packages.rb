require 'gitlab'

Gitlab.configure do |config|
  config.endpoint       = 'https://gitlab.com/api/v4'
  config.private_token  = ''
end

Gitlab.group_projects('archlinux-pkg/packages', {
    simple: true,
  order_by: 'name',
      sort: 'asc',
  per_page: 100,
}).auto_paginate.each do |project|
  puts "#{project.name}"
end
